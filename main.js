const electron = require('electron')
const app = electron.app
const BrowserWindow = electron.BrowserWindow
const path = require('path')
const url = require('url')
const dialog = require('electron').dialog;
const fs = require('fs');
var ToSSH = require('to-ssh');
var ipc = require('electron').ipcMain;
var connect = require('./server/ssh.js');
let mainWindow

function createWindow () {
  mainWindow = new BrowserWindow({width: 800, height: 600})

  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, './client/views/layouts/main.html'),
    protocol: 'file:',
    slashes: true
  }))

  mainWindow.on('closed', function () {
    mainWindow = null
  })

  mainWindow.webContents.openDevTools()
  // console.log(dialog.showOpenDialog({ properties: [ 'openFile', 'openDirectory', 'multiSelections' ]}));

}

app.on('ready', createWindow)

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  if (mainWindow === null) {
    createWindow()
  }
})

exports.openWindow = function () {
  let win = new BrowserWindow({width: 400, height: 300})
  win.loadURL(url.format({
    pathname: path.join(__dirname, './client/views/help.html'),
    protocol: 'file:',
    slashes: true
  }))
}

exports.saveFile = function (file) {
  console.log(file);
}
let ruta;

exports.dirFile = function () {
  dialog.showOpenDialog({ properties: [ 'openFile', 'openDirectory', 'multiSelections' ]}
  , function(dir){
    console.log(dir);
    ruta = dir[0];
  });
}

// exports.testReturn = function () {
//   return 1;
// }

exports.uploadFile = function () {
  console.log(ruta,'segunda fase');
  dialog.showOpenDialog({
  properties: [ 'openFile' ] }, function ( filename ) {
    var rutaG = filename[0];
    filename = filename[0].split('/');
    var nameFile = filename[filename.length - 1];
    fs.readFile(rutaG, {encoding: 'utf-8'}, function(err,data){
        if (!err) {
          fs.writeFile(ruta + '/' + nameFile, data, function(err) {
            if(err) {
                return console.log(err);
            }
            console.log("The file was saved!");
          });
        } else {
            console.log(err);
        }
    });
  });
}

ipc.on('connection-ssh', function(event, arg) {
  console.log(arg);
  var options = {
    host :arg.ip,
    username : arg.user,
    password : arg.password
  };
  var ssh = new ToSSH(options);
  ssh.connect(function(error) {
      if(!error) {
        console.log("Connected!"); // -> "Connected!"
        var result = {
          err : false,
          host : arg.ip,
          username : arg.user
        };
        event.sender.send('connection-ssh-res', result)
        //ssh.disconnect();
      } else {
        var result = {
          err: true
        }
        event.sender.send('connection-ssh-res', result)
      }
  });
});
