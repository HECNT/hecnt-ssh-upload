var app = angular.module("myApp", []);
const remote = require('electron').remote
const main = remote.require('./main.js');
var ipc = require('electron').ipcRenderer;

     app.directive('fileModel', ['$parse', function ($parse) {
        return {
           restrict: 'A',
           link: function(scope, element, attrs) {
              var model = $parse(attrs.fileModel);
              var modelSetter = model.assign;
              element.bind('change', function(){
                 scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                 });
              });
           }
        };
     }]);
     app.service('Service', ['$http', function ($http) {

     }]);


     app.controller('myCtrl', ['$scope', '$location', 'Service', function($scope, $location,Service){
        $scope.uploadFile = function(){
           var file = $scope.myFile;
           console.log(file);
           main.saveFile(file);
        };

        $scope.btnUpload = function(){
          main.uploadFile();
        };

        $scope.btnRuta = function () {
          var res = main.dirFile();
          console.log(res);
        }

        $scope.btnSsh = function () {
          $(document).ready(function(){
            $('.ui.modal').modal('show');
          });
        }

        $scope.doSsh = function (item) {
          console.log(item);
          var data = item;
          // SERVICE
          ipc.send('connection-ssh', data);
          ipc.on('connection-ssh-res', (event, result) => {
            // RESPONSE
            if (result.err) {
              console.log('hubo un error');
            } else {
              console.log('todo bien');
            }
          })
        }

        console.log($location.absUrl(),'urlssss');
        // $scope.testReturn = function () {
        //   console.log(main.testReturn());
        // }
     }]);
